let codigo;
let notasELiminar;
let datosEstudiantes;
let estudiante;

async function leerJSON(url) {
    try {
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch (err) {
        alert(err);
    }
}

function obtenerCodigo() {
    var paramstr = window.location.search.substr(1);
    var paramarr = paramstr.split("&");
    var params = {};

    for (var i = 0; i < paramarr.length; i++) {
        var tmparr = paramarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    codigo = params["codigo"];
    notasELiminar = params["eliminar"];
    let eti_codigo = document.getElementById("codigo");
    eti_codigo.innerHTML = codigo;
}

let eti_codigo = document.getElementById("codigo");
eti_codigo.innerHTML = codigo;

document.addEventListener("DOMContentLoaded", function () {
    obtenerCodigo();
    let url =
        "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json";
    estudiantes = leerJSON(url).then((datos) => {
        datosEstudiantes = datos;
        let eti_materia = document.getElementById("materia");
        eti_materia.innerHTML = datos.nombreMateria;
        var student = buscarEstudiante(datos.estudiantes, codigo);
        let nombre = document.getElementById("nombre");
        nombre.innerHTML = student.nombre;
        var datosTabla = organizarTabla(student);
        google.charts.load("current", { packages: ["table"] });
        google.charts.setOnLoadCallback(drawTable);

        function drawTable() {
            var data = new google.visualization.DataTable();
            data.addColumn("string", "Descripcion");
            data.addColumn("number", "Valor");
            data.addColumn("string", "Observación");
            data.addRows(datosTabla);

            var table = new google.visualization.Table(
                document.getElementById("table_div")
            );

            table.draw(data, { showRowNumber: false, width: "60%%", height: "60%" });
        }
        crearDiagramaCircular(datosTabla);
    });
});

function crearDiagramaCircular(datosTabla) {
    google.charts.load("current", { packages: ["corechart"] });
    google.charts.setOnLoadCallback(drawChart);
    let porcentaje = porcentajeNotas(datosTabla);
    function drawChart() {
        
        var data = google.visualization.arrayToDataTable([
            ["Notas", "Juicio de valor"],
            ["Notas Aprobadas", porcentaje[0]],
            ["Notas Reprobadas", porcentaje[1]],
        ]);

        var options = {
            title: "Porcentaje de Notas",
        };

        var chart = new google.visualization.PieChart(
            document.getElementById("piechart")
        );

        chart.draw(data, options);
    }
}

function organizarTabla(estudiante) {
    let notas = [];
    for (n in estudiante.notas) {
        let individual = [];
        individual.push("Nota N° " + estudiante.notas[n].id);
        individual.push(estudiante.notas[n].valor);
        individual.push("0");
        notas.push(individual);
    }
    let aux = [];
    eliminarNotas(notas);
    aux.push("Nota Definitiva:");
    aux.push(calcularPromedio(notas));
    aux.push("Nota Definitiva:");
    notas.push(aux);
    console.log(notas)
    return notas;
}

function eliminarNotas(notas) {
    let eliminadas = 0;
    let menor = 5;
    let actual = 0;
    let posicion = 0;
    for (n in notas) {
        menor = 5;
        for (let i = n; i < notas.length; i++) {
            actual = notas[i][1];
            if (actual < menor && notas[i][2] == "0" && eliminadas < notasELiminar) {
                menor = actual;
                posicion = i;
            }
        }
        notas[posicion][2] = "Nota eliminada";
        eliminadas++;
    }
    completarnotas(notas);
}

function completarnotas(notas) {
    for (n in notas) {
        if (notas[n][2] == "0") {
            if (notas[n][1] >= 3.0) {
                notas[n][2] = "Nota aprobada";
            } else {
                notas[n][2] = "Nota reprobada";
            }
        }
    }
}

function calcularPromedio(notas) {
    let promedio = 0;
    let i = 0;
    for (n in notas) {
        if (notas[n][2] != "Nota eliminada") {
            promedio += notas[n][1];
            i++;
        }
    }
    return promedio / i;
}

function buscarEstudiante(estudiantes, codigo) {
    for (e in estudiantes) {
        if (estudiantes[e].codigo == codigo) {
            return estudiantes[e];
        }
    }
    return null;
}

function porcentajeNotas(notas){
    let porcentaje=[];
    let aprobadas = 0;
    let reprobadas= 0 ;
    for(n in notas){
        if(notas[n][2] == "Nota aprobada"){
            aprobadas++;
        }
        else if(notas[n][2] == "Nota reprobada"){
            reprobadas++;
        }
    }
    porcentaje.push(aprobadas);
    porcentaje.push(reprobadas);
    return porcentaje;
}