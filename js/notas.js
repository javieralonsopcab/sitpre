let datosEstudiantes = [];

async function leerJSON(url) {
    try {
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch (err) {
        alert(err);
    }
}

document.addEventListener("DOMContentLoaded", function () {
    let url = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json"
    estudiantes = leerJSON(url).then(datos => {
        datosEstudiantes = datos;
    });
})

function consultarNotas() {
    const notas = (Number)(document.getElementById("notas-eliminar").value);
    const codigoEstudiante = (Number)(document.getElementById("codigo-estudiante").value);
    window.location = (`html/mostrarNotas.html?codigo=${codigoEstudiante}&eliminar=${notas}`);
    alert();
}

function verMiAutoevaluacion() {
    let codigo = document.getElementById("codigo-estudiante");
    let estudiante = buscarEstudiante(datosEstudiantes.estudiantes, codigo.value)
    codigoEstudiante = codigo;
    let promedio = calcularPromedio(estudiante.notas);
    alert("Mi nota es de : "+promedio)
}

function calcularPromedio(notas) {
    let promedio = 0;
    for (n in notas) {
        promedio += notas[n].valor;
    }
    return (promedio/notas.length).toFixed(2);
}

function buscarEstudiante(estudiantes, codigo) {
    for (e in estudiantes) {
        if (estudiantes[e].codigo == codigo) {
            return estudiantes[e];
        }
    }
    return null;
}
